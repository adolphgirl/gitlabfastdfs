package com.reyes.fastdfs.model.dao;

import java.io.Serializable;

public interface BaseDAO <T, ID extends Serializable> {

	/**
	 * 儲存對象
	 * @param entity
	 * @return
	 */
	public boolean save(T entity);
	
	/**
	 * 更新對象
	 * @param entity
	 * @return
	 */
	public boolean update(T entity);
	
	/**
	 * 刪除對象
	 * @param entity
	 * @return
	 */
	public boolean delete(T entity);
	
	/**
	 * 根據id查詢對象
	 * @param entity
	 * @param id
	 * @return
	 */
	public T findById(T entity, Long id);
	
	/**
	 * 使用JPQL sql查詢
	 * @param sql		sql語句
	 * @param param		查詢條件參數
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public java.util.List findBySQL(String sql, java.util.Map<String, Object> param);
	
	/**
	 * 使用JPQL sql查詢，取出特定範圍結果
	 * @param sql		sql語句
	 * @param param		查詢條件參數
	 * @param begin		查詢結果回傳起始值
	 * @param max		查詢結果回傳最大筆數值
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public java.util.List findBySQL(String sql, java.util.Map<String, Object> param, int begin, int max); 
	
}
