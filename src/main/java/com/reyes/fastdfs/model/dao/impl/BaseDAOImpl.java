package com.reyes.fastdfs.model.dao.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.reyes.fastdfs.model.dao.BaseDAO;

import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Logger;

@Repository
public class BaseDAOImpl<T, ID extends Serializable> implements BaseDAO<T, ID>{
	
	private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public boolean save(T entity) {
		boolean flag = false;
		
		try{
			entityManager.persist(entity);
			flag = true;
		}catch(Exception x){
			flag = false;
			logger.error("[+] [BaseDAOImpl]-[save]-[發生未預期錯誤]", x);
		}
		
		return flag;
	}

	@Override
	public boolean update(T entity) {
		boolean flag = false;
		
		try{
			entityManager.merge(entity);
			flag = true;
		}catch(Exception x){
			flag = false;
			logger.error("[+] [BaseDAOImpl]-[update]-[發生未預期錯誤]", x);
		}
		
		return flag;
	}

	@Override
	public boolean delete(T entity) {
		boolean flag = false;
		
		/**先更新目前物件生命周期的Managed狀態*/
		try{
			entityManager.remove(entityManager.merge(entity));
			flag = true;
		}catch(Exception x){
			flag = false;
			logger.error("[+] [BaseDAOImpl]-[delete]-[發生未預期錯誤]", x);
		}
		
		return flag;
	}

	@Override
	public T findById(T entity, Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List findBySQL(String sql, Map<String, Object> param) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List findBySQL(String sql, Map<String, Object> param, int begin, int max) {
		// TODO Auto-generated method stub
		return null;
	}

}
