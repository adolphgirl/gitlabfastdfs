package com.reyes.fastdfs.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * spring boot configuration use annotation
 * 讀取設定檔轉換為class的方式
 */
@Component
@ConfigurationProperties(prefix = "self")
public class SelfConfig {
	
	private String name;
	private String localtion;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocaltion() {
		return localtion;
	}

	public void setLocaltion(String localtion) {
		this.localtion = localtion;
	}
	
	
	
}
