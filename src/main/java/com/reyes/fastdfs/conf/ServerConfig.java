package com.reyes.fastdfs.conf;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.reyes.fastdfs.filter.EchoFilterBean;

@Configuration
public class ServerConfig {

	@Bean
	public FilterRegistrationBean<EchoFilterBean> echoFilter(){
		FilterRegistrationBean<EchoFilterBean> frb = new FilterRegistrationBean<EchoFilterBean>(new EchoFilterBean());
		frb.addUrlPatterns("/*");
		return frb;
		
	}
	
	@Bean
	public FilterRegistrationBean<CharacterEncodingFilter> encodeFilter(){
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setForceEncoding(true);
		characterEncodingFilter.setEncoding("UTF-8");
		
		FilterRegistrationBean<CharacterEncodingFilter> encodingFilter = new FilterRegistrationBean<CharacterEncodingFilter>();
		encodingFilter.setFilter(characterEncodingFilter);
		encodingFilter.addUrlPatterns("/*");
		return encodingFilter;
	}
	
}
