package com.reyes.fastdfs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
//@EnableTransactionManagement 預設已經開啟，可以不需要加入
@ServletComponentScan(basePackages = {"com.reyes.fastdfs.servlet", "com.reyes.fastdfs.filter"})
public class FastDfsTutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(FastDfsTutorialApplication.class, args);
	}

}
