package com.reyes.fastdfs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.reyes.fastdfs.model.bo.User;
import com.reyes.fastdfs.model.dao.BaseDAO;
import com.reyes.fastdfs.service.HomeService;

@Service
public class HomeServiceImpl implements HomeService {
	
	@Autowired
	private BaseDAO<User, Long> baseDAO;
	
	/**
	 * @Transactional
	 * 沒加的情況，No EntityManager with actual transaction available for current thread - cannot reliably process 'persist' call
	 */
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void createTestUser(String name, long age) throws Exception {
		this.baseDAO.save(new User("A", 10));
		this.baseDAO.save(new User("B", 10));
		this.baseDAO.save(new User("C", 10));
		
		// 故意發生異常
		this.baseDAO.save(new User("DDDDDD", 10));
		
		this.baseDAO.save(new User("E", 10));
		this.baseDAO.save(new User("F", 10));
	}

}
