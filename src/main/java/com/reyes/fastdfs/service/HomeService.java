package com.reyes.fastdfs.service;

public interface HomeService {
	
	/**
	 * 事務管理測試
	 * @param name
	 * @param age
	 * @throws Exception
	 */
	public void createTestUser(String name, long age) throws Exception;
	
}
