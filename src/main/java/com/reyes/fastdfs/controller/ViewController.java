package com.reyes.fastdfs.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.reyes.fastdfs.model.bo.User;
import com.reyes.fastdfs.model.bo.ValidBean;

@Controller
public class ViewController {
	
	@GetMapping("/jsp")
	public String jspPage(Model model, @RequestParam String name) {
		model.addAttribute("name", name);
		return "jsp/sample";
	}

	@GetMapping("/thymeleaf")
	public String thymeleafPage(Model model, @RequestParam String name) {
		model.addAttribute("name", name);
		
		User User = new User();
		User.setName("陳文得");
		User.setAge(102354);
		model.addAttribute("user", User);
		
		List<User> endList = new ArrayList<User>(10);
		for(int i=0, ilen=10; i<ilen; i++){
			User dtl = new User();
			dtl.setName(i + "A");
			dtl.setAge(i * 10 + 2);
			endList.add(dtl);
		}
		model.addAttribute("userList", endList);
		
		Map<String, String> map = new HashMap<String, String>(4);
		map.put("BGHC", "天天天BGHC");
		map.put("BGBH", "天天天BGBH");
		map.put("BAAA", "天天天BAAA");
		map.put("BNNN", "天天天BNNN");
		model.addAttribute("userMap", map);
		
		String[] arr = null;
		model.addAttribute("userArr", arr);
		
//		model.addAttribute("sex", "1");
		
		return "thymeleaf/sample";
	}
	
	@RequestMapping(value = "/bindresult", method = RequestMethod.GET)
	public String bindresult(Model model, @ModelAttribute(value = "validBean") ValidBean validBean){
		System.out.println(validBean);
		return "thymeleaf/bindresult";
	}
	
}
