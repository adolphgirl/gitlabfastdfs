package com.reyes.fastdfs.controller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.reyes.fastdfs.model.bo.User;

import ch.qos.logback.classic.Logger;

@RestController
public class RESTfulController {
	
	private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());
	
	/**spring boot redisTemplate 為配置後自動生成RedisTemplate這個Bean*/
	@Autowired
	private RedisTemplate<Object, Object> redisTemplate;

	@RequestMapping(method = RequestMethod.GET, path = "/boot/rest/user/{id}")
	public Object genUser(@PathVariable(name = "id") Long id){
		RedisSerializer<String> key = RedisSerializer.string();
		redisTemplate.setKeySerializer(key);
		
		User user = (User)redisTemplate.opsForValue().get("iduser");
		if(user != null){
			logger.info("[+] [RESTfulController]-[genUser]-[redis get user id is: " + user.getId() + "]");
			return user;
		}
		
		user = new User();
		user.setId(id);
		user.setName("No name");
		
		redisTemplate.opsForValue().set("iduser", user);
		
		return user;
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/boot/rest/user/{id}/{name}")
	public Object genUser(@PathVariable(name = "id") Long id, @PathVariable(name = "name") String name){
		User user = new User();
		user.setId(id);
		user.setName(name);
		return user;
	}
	
}
