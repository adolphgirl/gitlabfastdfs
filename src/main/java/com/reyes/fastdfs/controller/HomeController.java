package com.reyes.fastdfs.controller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.reyes.fastdfs.conf.SelfConfig;
import com.reyes.fastdfs.service.HomeService;

import ch.qos.logback.classic.Logger;

@RestController
public class HomeController {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private SelfConfig selfConfig;
	
	@Autowired
	private HomeService homeService;

	@GetMapping(value = "/")
	public String index() {
		return selfConfig.getName() + " in " + selfConfig.getLocaltion();
	}

	// /**另一種方式*/
	// @RequestMapping(value = "/aa", method = RequestMethod.GET)
	// public String indexA(){
	// return selfConfig.getName() + " in " + selfConfig.getLocaltion();
	// }

	@GetMapping(value = "/create")
	public String createTestUser(@RequestParam(name = "name") String name, @RequestParam(name = "age") String age) {
		String res = null;
		try {
			homeService.createTestUser(name, Long.parseLong(age));
			res = "success";
		} catch (Exception x) {
			logger.error("[+] [HomeController]-[createTestUser]-[發生未預期錯誤]", x);
			res = "error";
		}

		return res;
	}
	
	@GetMapping(value = "/encode/{name}")
	public String encode(@PathVariable(name = "name") String name) {
		logger.info("[+] [HomeController]-[encode]-[{}]", name);
		return name;
	}
	
}
